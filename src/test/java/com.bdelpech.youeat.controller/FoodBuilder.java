package com.bdelpech.youeat.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class FoodBuilder {
    public static List<String> getIds() {
        return Collections.singletonList("1");
    }

    public static FoodDto getDto() {
        FoodDto dto = new FoodDto();
        dto.setId("1");
        return dto;
    }
}