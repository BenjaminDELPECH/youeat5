package com.bdelpech.youeat.controller;

import ch.qos.logback.core.joran.util.beans.BeanUtil;
import com.bdelpech.youeat.controller.CustomUtils;
import com.bdelpech.youeat.controller.FoodController;
import com.bdelpech.youeat.dto.FoodDto;
import com.bdelpech.youeat.entities.Food;
import com.bdelpech.youeat.mapper.EntityMapper;
import com.bdelpech.youeat.mapper.FoodMapper;
import com.bdelpech.youeat.service.FoodService;
import org.hamcrest.Matchers;
import org.hamcrest.core.Is;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;

@Transactional
public class FoodControllerTest {
    private static final String ENDPOINT_URL = "/api/food";
    @InjectMocks
    private FoodController foodController;
    @Mock
    private FoodService foodService;
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(foodController)
                //.setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                //.addFilter(CustomFilter::doFilter)
                .build();
    }

    @Test
    public void findAllByPage() throws Exception {
        Page<FoodDto> page = new PageImpl<>(Collections.singletonList(FoodBuilder.getDto()));

        Mockito.when(foodService.findByCondition(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(page);

        ResultActions resultActions = mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT_URL)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.data.content", Matchers.hasSize(1)));

        Mockito.verify(foodService, Mockito.times(1)).findByCondition(ArgumentMatchers.any(), ArgumentMatchers.any());
        Mockito.verifyNoMoreInteractions(foodService);

    }

    @Test
    public void getById() throws Exception {
        Mockito.when(foodService.findById(ArgumentMatchers.anyLong())).thenReturn(FoodBuilder.getDto());

        mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT_URL + "/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Is.is(1)));
        Mockito.verify(foodService, Mockito.times(1)).findById("1");
        Mockito.verifyNoMoreInteractions(foodService);
    }

    @Test
    public void save() throws Exception {
        Mockito.when(foodService.save(ArgumentMatchers.any(FoodDto.class))).thenReturn(FoodBuilder.getDto());

        mockMvc.perform(
                        MockMvcRequestBuilders.post(ENDPOINT_URL)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(CustomUtils.asJsonString(FoodBuilder.getDto())))
                .andExpect(MockMvcResultMatchers.status().isCreated());
        Mockito.verify(foodService, Mockito.times(1)).save(ArgumentMatchers.any(FoodDto.class));
        Mockito.verifyNoMoreInteractions(foodService);
    }

    @Test
    public void update() throws Exception {
        Mockito.when(foodService.update(ArgumentMatchers.any(), ArgumentMatchers.anyLong())).thenReturn(FoodBuilder.getDto());

        mockMvc.perform(
                        MockMvcRequestBuilders.put(ENDPOINT_URL + "/1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(CustomUtils.asJsonString(FoodBuilder.getDto())))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(foodService, Mockito.times(1)).update(ArgumentMatchers.any(FoodDto.class), ArgumentMatchers.anyLong());
        Mockito.verifyNoMoreInteractions(foodService);
    }

    @Test
    public void delete() throws Exception {
        Mockito.doNothing().when(foodService).deleteById(ArgumentMatchers.anyLong());
        mockMvc.perform(
                MockMvcRequestBuilders.delete(ENDPOINT_URL + "/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(CustomUtils.asJsonString(FoodBuilder.getIds()))).andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(foodService, Mockito.times(1)).deleteById(Mockito.anyLong());
        Mockito.verifyNoMoreInteractions(foodService);
    }
}