package com.bdelpech.youeat.repository;

import com.bdelpech.youeat.entities.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource(collectionResourceRel = "foods", path="foods")
public interface FoodRepository extends JpaRepository<Food,Integer> {
}