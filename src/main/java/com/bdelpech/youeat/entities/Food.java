package com.bdelpech.youeat.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "food")
@Data
public class Food {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

}
